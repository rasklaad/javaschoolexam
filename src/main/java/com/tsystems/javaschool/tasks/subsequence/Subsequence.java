package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x ==null || y == null)
            throw new IllegalArgumentException();

        if (x.size() > y.size())
            return false;

        boolean temp = false;
        ListIterator iterator = y.listIterator();
        for (Object xItem:x){
            while (iterator.hasNext()){
                Object yItem = iterator.next();
                if(xItem.hashCode() == yItem.hashCode()) {
                    temp = true;
                    break;                                      //item has been found, go to the next item in x list
                }

            //this happens, when list y has no items, while list x still has items, that have not been found in list y
                if (!iterator.hasNext())
                    return false;
            }
            if (!temp)
                return false;
        }
        return true;
    }
}
