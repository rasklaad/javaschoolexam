package com.tsystems.javaschool.tasks.pyramid;

import com.tsystems.javaschool.tasks.calculator.Calculator;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        int rows = getRowsQtty(inputNumbers.size());
        int columns = getColumnsQtty(inputNumbers.size());

        Collections.sort(inputNumbers);
        Collections.reverse(inputNumbers);
        ListIterator<Integer> iterator = inputNumbers.listIterator();
        int [][] pyramid;
        pyramid = new int [rows][columns];
        int offset = 0;
        for (int i = rows-1; i >= 0; i--){
            for (int j = columns-1-offset; j >= offset; j = j-2){
                int lastItem = iterator.next();
                pyramid[i][j] = lastItem;
            }
            offset++;
        }
        if (iterator.hasNext())
            throw new CannotBuildPyramidException();

        return pyramid;
    }

    //-x^2-x+2y=0
    private int getRowsQtty(int numbersQtty){
        int discriminant = 8 * numbersQtty;
        discriminant++;
        int x1 = (int)(-1-Math.sqrt(discriminant))/2;
        int x2 = (int)(-1+Math.sqrt(discriminant))/2;
        if (x1 > 0)
            return x1;
        else if (x2 > 0)
            return x2;
        else throw new CannotBuildPyramidException();
    }

    // -x^2-4x+8y-3=0
    private int getColumnsQtty(int numbersQtty){
        Calculator calculator = new Calculator();
        int discriminant = Integer.valueOf(calculator.evaluate("4*(8*"+numbersQtty+"-3)+16"));
        int x1 = -Double.valueOf(calculator.evaluate("(4+"+Math.sqrt(discriminant)+")/2")).intValue();
        int x2 = Double.valueOf(calculator.evaluate("("+Math.sqrt(discriminant)+"-4)/2")).intValue();
        if (x1 > 0)
            return x1;
        else if (x2 > 0)
            return x2;
        else throw new CannotBuildPyramidException();
    }
}
