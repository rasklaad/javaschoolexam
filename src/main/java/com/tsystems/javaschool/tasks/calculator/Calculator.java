package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate2(String statement) {
        if (statement == null || statement.equals("") || statement.contains(","))
            return null;

        Pattern p = Pattern.compile(".*(\\.{2,}|-{2,}|\\+{2,}|\\*{2,}|[/]{2,}).*");
        Matcher matcher = p.matcher(statement);
        if (matcher.matches())
            return null;

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        String result;
        try {
            result = engine.eval(statement).toString();
            if (result.contains(".")) {
                String[] resultingArray = result.split("\\.");
                int endIndex = 4;
                if (resultingArray[1].length() < 4)
                    endIndex = resultingArray[1].length();
                result = resultingArray[0] + "." + resultingArray[1].substring(0, endIndex);
            }
            if (result.equals("Infinity"))
                return null;

        }
        catch (ScriptException e){
            return null;
        }
        return result;
    }

    private static boolean isHigher(String token, String operator)
    {
        return (operators.containsKey(operator) && operators.get(operator) >= operators.get(token));
    }

    private static Map<String, Integer> operators = new HashMap<String, Integer>() {{
        put("+", 1);
        put("-", 2);
        put("*", 3);
        put("/", 4);
    }};

    public String evaluate(String statement) {

        if (statement == null || statement.equals("") || statement.contains(","))
            return null;

        Pattern p = Pattern.compile(".*(\\.{2,}|-{2,}|\\+{2,}|\\*{2,}|[/]{2,}).*");
        Matcher matcher = p.matcher(statement);
        if (matcher.matches())
            return null;

        Pattern parentheses = Pattern.compile("[()]");
        matcher = parentheses.matcher(statement);
        int parenthesesCount = 0;
        while (matcher.find())
            parenthesesCount++;
        if (parenthesesCount%2 != 0)
            return null;

        String regexPattern = "(\\+|[/]|-|\\*|\\(|\\))";

        LinkedList<String> postfix = new LinkedList<>();
        Stack<String> stack  = new Stack<>();

        //Split digits and operators to array, including delimiters.
        String [] infix = statement.split("(?<="+regexPattern+")|" +
                                        "(?="+regexPattern+")");

        //Convert infix statement to reverse polish notation statement.
        for (String token : infix) {

            if (operators.containsKey(token)) {
                while ( ! stack.isEmpty() && isHigher(token, stack.peek()))
                    postfix.addLast(stack.pop());
                stack.push(token);

            } else if (token.equals("(")) {
                stack.push(token);

            } else if (token.equals(")")) {
                while ( ! stack.peek().equals("("))
                    postfix.addLast(stack.pop());
                stack.pop();

            } else {
                postfix.addLast(token); //digit
            }
        }

        //Add remaining tokens to the reverse polish notation statement.
        while ( ! stack.isEmpty())
            postfix.addLast(stack.pop());

        //Calculate expression.
        ListIterator<String> iterator = postfix.listIterator();
        while (iterator.hasNext()){
            String item = iterator.next();
            if (operators.containsKey(item)){
                String result = "";
                iterator.previous();
                BigDecimal operand2;
                BigDecimal operand1;
                try {
                    operand2 = new BigDecimal(iterator.previous());
                    operand1 = new BigDecimal(iterator.previous());
                }
                catch (NumberFormatException e) {
                    return null;
                }
                switch(item) {
                    case "+":
                        result = operand1.add(operand2).toString();
                        break;
                    case "-":
                        result = operand1.subtract(operand2).toString();
                        break;
                    case "*":
                        result = operand1.multiply(operand2).toString();
                        break;
                    case "/":
                        if (operand2.toString().equals("0"))
                            return null;
                        result = operand1.divide(operand2, 15, BigDecimal.ROUND_UP).toString();
                        break;
                }
                iterator.remove(); //delete operand 1
                iterator.next();
                iterator.remove(); //delete operand 2
                iterator.next();
                iterator.remove(); //delete operator
                iterator.add(result);
                iterator = postfix.listIterator();
            }
        }
        String evaluationResult = postfix.getFirst();
        DecimalFormat decimalFormat = new DecimalFormat("0.####");
        return decimalFormat.format(Double.valueOf(evaluationResult));
    }
}
